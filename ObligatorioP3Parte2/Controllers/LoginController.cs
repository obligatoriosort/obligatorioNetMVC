﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ObligatorioP3Parte2.Models.Dominio;

namespace ObligatorioP3Parte2.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }
        // POST: Usuario/Login
        [HttpPost]
        public ActionResult Login(string EmailLogin, string PasswordLogin)
        {
            try
            {
                using (OficinaContext db = new OficinaContext())
                {
                    var usr = db.Usuario.SingleOrDefault
                        (u => u.email.ToUpper() == EmailLogin.ToUpper()
                        && u.password == PasswordLogin);
                    if (usr != null)
                    {

                        Session["Usuario"] = usr as Usuario;

                        return RedirectToAction("Index", "Home");

                    }
                    ModelState.AddModelError("LoginIncorrecto", "El mail o contraseña son inexistentes");
                    return View("Error5");
                }
            }
            catch
            {
                return View();
            }
        }
        public ActionResult Logout()
        {
            Session["Usuario"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}