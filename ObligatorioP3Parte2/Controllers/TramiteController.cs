﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ObligatorioP3Parte2.Models.Dominio;
using ObligatorioP3Parte2.Models.Dominio.ViewModels;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Configuration;

namespace ObligatorioP3Parte2.Controllers
{
    public class TramiteController : Controller
    {

        public ActionResult ListaTramites()
        {
            return View(new ObligatorioP3Parte2.Models.Dominio.ViewModels.ExpedienteViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListaTramites(ExpedienteViewModel ViewModel)
        {

            if (ViewModel != null)
            {
                using (OficinaContext db = new OficinaContext())
                {
                    var t = db.Tramite.Where(p => p.idTramite == ViewModel.IdTram);
                    Tramite tr = t.FirstOrDefault();
                    return RedirectToAction("ListaEtapas", tr);
                }
            }
            return View();
        }

        public ActionResult ListaEtapas(Tramite tramite)
        {
            using (OficinaContext db = new OficinaContext())
            {
                Tramite tr = tramite;
                var et = db.FinalizacionEtapa.Where(p => p.etapa.tramite.idTramite == tr.idTramite).Include(p=> p.etapa);
            
                return View(et.ToList());
            }

        }

        //public ActionResult CrearFoto(string foto)
        //{
        //    string ruta = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaFoto"] + foto;
          
        //}
    }
}
