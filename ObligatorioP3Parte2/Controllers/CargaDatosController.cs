﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Configuration;
using ObligatorioP3Parte2.Models.Dominio;

namespace ObligatorioP3Parte2.Controllers
{
    public class CargaDatosController : Controller
    {
        // GET: CargaDatos
        private OficinaContext db = new OficinaContext();
        public ActionResult CargaGrupos()
        {
            string ruta = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaGrupo"];
            if (System.IO.File.Exists(ruta))
            {
                string[] lines = System.IO.File.ReadAllLines(ruta);
                if (lines != null)
                {
                    List<Grupo> Grupos = new List<Grupo>();

                    foreach (string line in lines)
                    {
                        List<string> datos = line.Split('#').ToList();
                        if (datos.Count < 0)
                        {
                            continue;
                        }
                        string nombre = datos[0];
                        int codGrupo = 0;
                        int.TryParse(datos[1], out codGrupo);
                        var f = db.Grupos.Where(p => p.codGrupo == codGrupo);
                        Grupo gp = f.FirstOrDefault();
                        if (nombre.Length > 0 && codGrupo > 0 && gp == null)
                        {
                            Grupo g = new Grupo { nombre = nombre, codGrupo = codGrupo };
                            db.Grupos.Add(g);
                        }

                    }
                    db.SaveChanges();
                }
            }
            return RedirectToAction("CargaFuncionarios", "CargaDatos");
        }

        public ActionResult CargaFuncionarios()
        {
            string ruta = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaFuncionario"];
            if (System.IO.File.Exists(ruta))
            {
                string[] lines = System.IO.File.ReadAllLines(ruta);
                if (lines != null)
                {
                    List<Funcionario> Funcionarios = new List<Funcionario>();

                    foreach (string line in lines)
                    {
                        List<string> datos = line.Split('#').ToList();
                        if (datos.Count < 0)
                        {
                            continue;
                        }
                        string nom = datos[0];
                        string mail = datos[1];
                        string grupo = datos[2];

                        using (OficinaContext db = new OficinaContext())
                        {
                            var funcionario = db.Funcionario.Where(p => p.email == mail);
                            Funcionario fn = funcionario.FirstOrDefault();
                            if (fn == null)
                            {
                                var q = db.Grupos.Where(p => p.nombre == grupo);
                                Grupo g = q.FirstOrDefault();
                                if (nom.Length > 0 && mail.Length > 0 && g != null)
                                {
                                    Funcionario f = new Funcionario { nombre = nom, email = mail, Grupo = g };
                                    db.Funcionario.Add(f);
                                }
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            return RedirectToAction("CargaTramites", "CargaDatos");
        }

        public ActionResult CargaTramites()
        {

            string ruta = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaTramite"];
            string ruta2 = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaGruposTramite"];

            if (System.IO.File.Exists(ruta) && System.IO.File.Exists(ruta2))
            {
                string[] lines = System.IO.File.ReadAllLines(ruta);
                string[] lines2 = System.IO.File.ReadAllLines(ruta2);
                if (lines != null && lines2 != null)
                {
                    using (OficinaContext db = new OficinaContext())
                    {
                        foreach (string line in lines)
                        {
                            List<string> datos = line.Split('|').ToList();
                            if (datos.Count < 0)
                            {
                                continue;
                            }
                            int idTram = 0;
                            int.TryParse(datos[0], out idTram);
                            string titu = datos[1];
                            string descrip = datos[2];
                            decimal cost = 0;
                            decimal.TryParse(datos[3], out cost);
                            int time = 0;
                            int.TryParse(datos[4], out time);
                            List<Grupo> grupo = new List<Grupo>();
                            foreach (string line2 in lines2)
                            {
                                List<string> datos2 = line2.Split('$').ToList();
                                if (datos.Count < 0)
                                {
                                    continue;
                                }
                                int idtram2 = 0;
                                int.TryParse(datos2[1], out idtram2);
                                if (idtram2 == idTram)
                                {
                                    int codigoGrupo = 0;
                                    int.TryParse(datos2[0], out codigoGrupo);
                                    var gp = db.Grupos.Where(g => g.codGrupo == codigoGrupo);
                                    Grupo gpo = gp.FirstOrDefault();
                                    grupo.Add(gpo);
                                }
                            }
                            var tramite = db.Tramite.Where(p => p.idTramite == idTram);
                            Tramite tr = tramite.FirstOrDefault();
                            if (tr == null)
                            {
                                if (titu.Length > 0 && descrip.Length > 0 && idTram > 0 && cost > 0 && time > 0)
                                {
                                    Tramite t = new Tramite { idTramite = idTram, titulo = titu, descripcion = descrip, costo = cost, timeEjec = time, grupos = grupo };
                                    db.Tramite.Add(t);
                                }
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            return RedirectToAction("CargaEtapas", "CargaDatos");
        }

        public ActionResult CargaEtapas()
        {
            string ruta = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaEtapa"];
            if (System.IO.File.Exists(ruta))
            {
                string[] lines = System.IO.File.ReadAllLines(ruta);
                if (lines != null)
                {
                    foreach (string line in lines)
                    {
                        List<string> datos = line.Split('@').ToList();
                        if (datos.Count < 0)
                        {
                            continue;
                        }
                        int idTram = 0;
                        int.TryParse(datos[0], out idTram);
                        string codE = datos[1];
                        string descrip = datos[2];
                        int lap = 0;
                        int.TryParse(datos[3], out lap);
                        using (OficinaContext db = new OficinaContext())
                        {

                            var etapa = db.Etapa.Where(p => p.codigoEtapa == codE);
                            Etapa et = etapa.FirstOrDefault();
                            var tramite = db.Tramite.Where(f => f.idTramite == idTram);
                            Tramite tr = tramite.FirstOrDefault();
                            if (et == null && tramite != null)
                            {
                                if (idTram > 0 && codE.Length > 0 && descrip.Length > 0 && lap > 0)
                                {
                                    Etapa e = new Etapa { codigoEtapa = codE, descripcion = descrip, lapso = lap, tramite = tr };
                                    db.Etapa.Add(e);
                                }
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }

        //public ActionResult CargaGruposTramite()
        //{
        //    string ruta = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaGruposTramite"];
        //    if (System.IO.File.Exists(ruta))
        //    {
        //        string[] lines = System.IO.File.ReadAllLines(ruta);
        //        if (lines != null)
        //        {
        //            foreach (string line in lines)
        //            {
        //                List<string> datos = line.Split('$').ToList();
        //                if (datos.Count < 0)
        //                {
        //                    continue;
        //                }
        //                int codG = 0;
        //                int.TryParse(datos[0], out codG);
        //                int idTram= 0;
        //                int.TryParse(datos[1], out idTram);
        //                using (OficinaContext db = new OficinaContext())
        //                {

        //                    var grupo= db.Grupos.Where(p => p.codGrupo == codG);
        //                    Grupo gp = grupo.FirstOrDefault();
        //                    var tramite = db.Tramite.Where(f => f.idTramite == idTram);
        //                    Tramite tr = tramite.FirstOrDefault();
        //                    if (gp!= null && tramite != null)
        //                    {
        //                        if (idTram > 0 && codE.Length > 0 && descrip.Length > 0 && lap > 0)
        //                        {
        //                            Etapa e = new Etapa { codigoEtapa = codE, descripcion = descrip, lapso = lap, tramite = tr };
        //                            db.Etapa.Add(e);
        //                        }
        //                        db.SaveChanges();
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return RedirectToAction("Index", "Grupo");
        //}




        // GET: CargaDatos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CargaDatos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CargaDatos/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CargaDatos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CargaDatos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CargaDatos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CargaDatos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
