﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ObligatorioP3Parte2.Models.Dominio;
using ObligatorioP3Parte2.Models.Dominio.ViewModels;

namespace ObligatorioP3Parte2.Controllers
{
    public class ExpedientesController : Controller
    {
        private OficinaContext db = new OficinaContext();

        // GET: Expedientes
        public ActionResult Index()
        {
            return View(db.Expediente.ToList());
        }
        public ActionResult FiltroPorId(string id)
        {

            var expedientes = db.Expediente.Include(f => f.solcitante);
                                
            int idparseado = 0;
            int.TryParse(id, out idparseado);
            if (!String.IsNullOrEmpty(id))
            {
                expedientes = expedientes.Where(s => s.id==idparseado);
                return View(expedientes.ToList());
            }
            expedientes = db.Expediente.Include(f => f.solcitante).OrderByDescending(f =>f.solcitante.cedula).ThenByDescending(f=> f.fechaInicio);
    
            return View(expedientes.ToList());
        }

        public ActionResult FiltroPorCedula(string cedula)
        {
            var expedientes = db.Expediente.Include(f => f.solcitante);


            if (!String.IsNullOrEmpty(cedula))
            {
                expedientes = expedientes.Where(s => s.solcitante.cedula==cedula);
                return View("FiltroPorId",expedientes.ToList());
            }
            expedientes = db.Expediente.Include(f => f.solcitante).OrderByDescending(f => f.solcitante.cedula).ThenByDescending(f => f.fechaInicio);
            return View("FiltroPorId",expedientes.ToList());

        }

        public ActionResult FiltroPorFuncionario(string funcionario)
        {
            var expedientes = db.Expediente.Include(f => f.solcitante);        
            //expedientes =
            //             from exp in db.Expediente.Include("Solcitante")
            //             join EtapaFinal in db.FinalizacionEtapa on exp.id equals EtapaFinal.Expediente.id
            //             join func in db.Funcionario on EtapaFinal.funcionario.email equals func.email
            //             where EtapaFinal.funcionario.email == funcionario
            //             select exp;
            //expedientes = expedientes.Distinct();
          
            if (!String.IsNullOrEmpty(funcionario))
            {
                expedientes =
             from EtapaFinal in db.FinalizacionEtapa
             join func in db.Funcionario on EtapaFinal.funcionario.email equals func.email
             join exp in db.Expediente on EtapaFinal.Expediente.id equals exp.id
             where EtapaFinal.funcionario.email == funcionario
             select exp;
                expedientes = expedientes.Distinct();

                return View("FiltroPorId", expedientes.ToList());
            }
            expedientes = db.Expediente.Include(f => f.solcitante).OrderByDescending(f => f.solcitante.cedula).ThenByDescending(f => f.fechaInicio);
            return View("FiltroPorId", expedientes.ToList());

        }

        //public ActionResult FiltroPorExpedienteCulminado()
        //{
        //    var expedientes = db.Expediente.Include(f => f.solcitante);

        //    expedientes =
        //                  from EtapaFinal in db.FinalizacionEtapa 
        //                  join exp in db.Expediente.Include("Solcitante") on EtapaFinal.Expediente.id equals exp.id
        //                  select exp;
        //        expedientes = expedientes.Distinct();
            

        //    return View("FiltroPorId", expedientes.ToList());

        //}
        public ActionResult FiltroPorExpedienteCulminado()
        {
           
            var expedientes = db.Expediente.Include(f => f.solcitante);

            expedientes =
                          from exp in db.Expediente.Include("Tramite")
                          
                          select exp;
            expedientes = expedientes.Distinct();
            expedientes.ToList();
            List<Expediente> ExpCulminados = new List<Expediente>();
            foreach (Expediente ex in expedientes)
            {
                using (OficinaContext db = new OficinaContext())
                {

                    var tramite = from et in db.Tramite.Include("Etapas")
                                  where et.idTramite == ex.tramite.idTramite
                                  select et;
                    tramite = tramite.Distinct();
                    Tramite tram = tramite.FirstOrDefault();
                    List < Etapa > etapas = new List<Etapa>();
                    foreach (Etapa et in tram.etapas)
                    {
                        etapas.Add(et);
                    }
                    var fes = from fe in db.FinalizacionEtapa
                             where ex.id == fe.Expediente.id
                             select fe;
                    fes = fes.Distinct();
                    fes.ToList();


                    if (fes.Count() == etapas.Count())
                    {
                        ExpCulminados.Add(ex);
                    }
                } 
                
            }

            return View("FiltroPorId", ExpCulminados);

        }

        //public ActionResult FiltroPorExpedienteNoCulminado()
        //{
        //    var expedientes = db.Expediente.Include(f => f.solcitante);
        //    expedientes =
        //                from expediente in db.Expediente.Include("Solcitante")
        //                where !(from Etapa in db.FinalizacionEtapa
        //                        select Etapa.Expediente.id)
        //                        .Contains(expediente.id)
        //                select expediente;
        //    expedientes = expedientes.Distinct();


        //    return View("FiltroPorId", expedientes.ToList());

        //}

        public ActionResult FiltroPorExpedienteNoCulminado()
        {
            var expedientes = db.Expediente.Include(f => f.solcitante);

            expedientes =
                          from exp in db.Expediente.Include("Tramite")

                          select exp;
            expedientes = expedientes.Distinct();
            expedientes.ToList();
            List<Expediente> ExpCulminados = new List<Expediente>();
            foreach (Expediente ex in expedientes)
            {
                using (OficinaContext db = new OficinaContext())
                {

                    var tramite = from et in db.Tramite.Include("Etapas")
                                  where et.idTramite == ex.tramite.idTramite
                                  select et;
                    tramite = tramite.Distinct();
                    Tramite tram = tramite.FirstOrDefault();
                    List<Etapa> etapas = new List<Etapa>();
                    foreach (Etapa et in tram.etapas)
                    {
                        etapas.Add(et);
                    }
                    var fes = from fe in db.FinalizacionEtapa
                              where ex.id == fe.Expediente.id
                              select fe;
                    fes = fes.Distinct();
                    fes.ToList();


                    if (fes.Count() != etapas.Count())
                    {
                        ExpCulminados.Add(ex);
                    }
                }

            }

            return View("FiltroPorId", ExpCulminados);

        }



        // GET: Expedientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Expediente expediente = db.Expediente.Find(id);
            if (expediente == null)
            {
                return HttpNotFound();
            }
            return View(expediente);
        }
        public ActionResult Solicitante()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Solicitante(string cedula)
        {
            try
            {
                using (OficinaContext db = new OficinaContext())
                {
                    var sol = db.Solicitante.SingleOrDefault
                        (u => u.cedula.ToUpper() == cedula.ToUpper());

                    Solicitante solicitante = sol;  
                    if (sol == null)
                    {
                        return RedirectToAction("CreateSolicitante");

                    }
                    TempData["Solicitante"] = solicitante;
                    return RedirectToAction("ListaTramites");
                }
            }
            catch
            {
                return View();
            }
        }
        public ActionResult CreateSolicitante()
        {
            return View();
        }


        // POST: Expedientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSolicitante([Bind(Include = "cedula,nombre,apellido,telefono,email")] Solicitante solicitante)
        {
            if (ModelState.IsValid)
            {
                using (OficinaContext db = new OficinaContext())
                {
                    var sol = db.Solicitante.SingleOrDefault
                        (u => u.cedula.ToUpper() == solicitante.cedula.ToUpper());
                    if (sol == null)
                    {
                        db.Solicitante.Add(solicitante);
                        db.SaveChanges();
                        TempData["Solicitante"] = solicitante;
                        return RedirectToAction("ListaTramites");
                    }
                    else return View(solicitante);
                }
            }

            return View(solicitante);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,emailLogueado,fechaInicio")] Expediente expediente)
        {
            if (ModelState.IsValid)
            {
                db.Expediente.Add(expediente);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            return View(expediente);
        }


        // GET: Expedientes/Create
        public ActionResult Create()
        {
            return View();
        }

        //public ActionResult ListaTramites()
        //{
        //    using (OficinaContext db = new OficinaContext())
        //    {
        //        List<Tramite> tramites = new List<Tramite>();
        //        tramites = db.Tramite.ToList();
        //       List<SelectListItem> listado = new List<SelectListItem>();
        //        foreach (Tramite  t in tramites)
        //        {

        //            listado.Add(new SelectListItem() { Text = t.titulo, Value = t.idTramite.ToString() });

        //        }

        //        return View(listado);
        //    }
        //}
        public ActionResult ListaTramites()
        {
            
            return View(new ObligatorioP3Parte2.Models.Dominio.ViewModels.ExpedienteViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListaTramites(ExpedienteViewModel ViewModel)
        {

            if (ViewModel != null)
            {
                using (OficinaContext db = new OficinaContext())
                {
                    var t = db.Tramite.Where(p => p.idTramite == ViewModel.IdTram);
                    Tramite tr = t.FirstOrDefault();
                    Solicitante sol = TempData["Solicitante"] as Solicitante;
                    db.Solicitante.Attach(sol);
                    db.Entry(sol).State = EntityState.Unchanged; // modify si queremos q lo modifique
                    string email = (Session["Usuario"] as Usuario).email;
                    Expediente exp = new Expediente { solcitante = sol, tramite = tr, emailLogueado = email, fechaInicio = DateTime.Now };
                    db.Expediente.Add(exp);
                    db.SaveChanges();
                    return View("Solicitante");
                }
            }
            return View();
        }


        // GET: Expedientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Expediente expediente = db.Expediente.Find(id);
            if (expediente == null)
            {
                return HttpNotFound();
            }
            return View(expediente);
        }

        // POST: Expedientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,emailLogueado,fechaInicio")] Expediente expediente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(expediente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(expediente);
        }

        // GET: Expedientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Expediente expediente = db.Expediente.Find(id);
            if (expediente == null)
            {
                return HttpNotFound();
            }
            return View(expediente);
        }

        // POST: Expedientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Expediente expediente = db.Expediente.Find(id);
            db.Expediente.Remove(expediente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
