﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ObligatorioP3Parte2.Models.Dominio;
using ObligatorioP3Parte2.Models.Dominio.ViewModels;

namespace ObligatorioP3Parte2.Controllers
{
    public class EtapasController : Controller
    {
        private OficinaContext db = new OficinaContext();

        // GET: Etapas
        public ActionResult Index()
        {
            return View(db.Etapa.ToList());
        }

        // GET: Etapas/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Etapa etapa = db.Etapa.Find(id);
            if (etapa == null)
            {
                return HttpNotFound();
            }
            return View(etapa);
        }
        public ActionResult Expediente()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Expediente(int id)
        {

            using (OficinaContext db = new OficinaContext())
            {
              
                var expediente = db.Expediente.Where(e => e.id == id).Include(e => e.tramite).FirstOrDefault();
                Expediente ex = expediente;
                if (ex != null)
                {
                    var tramite = db.Tramite.Where(t => t.idTramite == ex.tramite.idTramite).Include(t => t.etapas).FirstOrDefault();
                    Tramite tr = tramite;
                    TempData["Tramite"] = tr;
                    TempData["Expediente"] = ex;
                    return RedirectToAction("ListaEtapa", tr);
                }
                else return View("Error3");
            }

        }
        public ActionResult ListaEtapa(Tramite tr)
        {
            int id = tr.idTramite;
            return View(new ObligatorioP3Parte2.Models.Dominio.ViewModels.EtapaViewModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListaEtapa(EtapaViewModel viewModel)
        {
            TempData["CodEtapa"] = viewModel.IdEtapa;

           return RedirectToAction("ListaFuncionarios");
        }
        public ActionResult ListaFuncionarios()
        {
            Tramite tr = TempData["Tramite"] as Tramite;
            return View(new ObligatorioP3Parte2.Models.Dominio.ViewModels.FuncionarioViewModel(tr));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListaFuncionarios(FuncionarioViewModel funViewModel)
        {
            using (OficinaContext db = new OficinaContext())
            {
                Expediente exp1 = TempData["Expediente"] as Expediente;
                var ex = db.Expediente.Where(p => p.id == exp1.id);
                Expediente exp = ex.FirstOrDefault();
                string codEtapa = TempData["CodEtapa"].ToString();
                var e = db.Etapa.Where(p => p.codigoEtapa == codEtapa);
                Etapa etap = e.FirstOrDefault();
                if (e != null)
                {    
                    var busqueda = from exprueba in db.FinalizacionEtapa
                                   where exprueba.Expediente.id == exp.id
                                   select exprueba;
                    List<FinalizacionEtapa> FeLista = busqueda.ToList();
                   
                    foreach(FinalizacionEtapa fer in FeLista)
                    {                     
                            if (fer.etapa == etap)
                            {
                                return View("Error2");
                            }
                    }

                }
                TimeSpan calculo = DateTime.Now.Date - exp.fechaInicio.Date;
                int dias = calculo.Days;
                bool finalizo = true; 
                if (dias > etap.lapso)
                {
                    finalizo = false;
                }
                var f = db.Funcionario.Where(p => p.email == funViewModel.email);
                Funcionario fu = f.FirstOrDefault();
                FinalizacionEtapa fe = new FinalizacionEtapa
                {
                    funcionario = fu,
                    etapa = etap,
                    Finalizo = finalizo,
                    imagen = funViewModel.imagen,
                    Expediente = exp,
                    fechaFinaliza = DateTime.Now.Date
                };
                db.FinalizacionEtapa.Add(fe);
                db.SaveChanges();
                if (dias > etap.lapso)
                {
                    return View("Error1");
                }

            }
            return RedirectToAction("Expediente");
        }

        // GET: Etapas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Etapas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "codigoEtapa,lapso,descripcion")] Etapa etapa)
        {
            if (ModelState.IsValid)
            {
                db.Etapa.Add(etapa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(etapa);
        }

        // GET: Etapas/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Etapa etapa = db.Etapa.Find(id);
            if (etapa == null)
            {
                return HttpNotFound();
            }
            return View(etapa);
        }

        // POST: Etapas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "codigoEtapa,lapso,descripcion")] Etapa etapa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(etapa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(etapa);
        }

        // GET: Etapas/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Etapa etapa = db.Etapa.Find(id);
            if (etapa == null)
            {
                return HttpNotFound();
            }
            return View(etapa);
        }

        // POST: Etapas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Etapa etapa = db.Etapa.Find(id);
            db.Etapa.Remove(etapa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
