namespace ObligatorioP3Parte2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracion3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Etapas", newName: "Etapa");
            RenameTable(name: "dbo.Tramites", newName: "Tramite");
            RenameTable(name: "dbo.Grupoes", newName: "Grupo");
            RenameTable(name: "dbo.Usuarios", newName: "Usuario");
            RenameTable(name: "dbo.Expedientes", newName: "Expediente");
            RenameTable(name: "dbo.FinalizacionEtapas", newName: "FinalizacionEtapa");
            RenameTable(name: "dbo.Funcionarios", newName: "Funcionario");
            RenameTable(name: "dbo.Solicitantes", newName: "Solicitante");
            RenameTable(name: "dbo.GrupoTramites", newName: "GrupoTramite");
            RenameTable(name: "dbo.UsuarioGrupoes", newName: "UsuarioGrupo");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.UsuarioGrupo", newName: "UsuarioGrupoes");
            RenameTable(name: "dbo.GrupoTramite", newName: "GrupoTramites");
            RenameTable(name: "dbo.Solicitante", newName: "Solicitantes");
            RenameTable(name: "dbo.Funcionario", newName: "Funcionarios");
            RenameTable(name: "dbo.FinalizacionEtapa", newName: "FinalizacionEtapas");
            RenameTable(name: "dbo.Expediente", newName: "Expedientes");
            RenameTable(name: "dbo.Usuario", newName: "Usuarios");
            RenameTable(name: "dbo.Grupo", newName: "Grupoes");
            RenameTable(name: "dbo.Tramite", newName: "Tramites");
            RenameTable(name: "dbo.Etapa", newName: "Etapas");
        }
    }
}
