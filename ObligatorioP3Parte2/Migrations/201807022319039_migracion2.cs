namespace ObligatorioP3Parte2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracion2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UsuarioGrupoes", "Usuario_email", "dbo.Usuarios");
            DropForeignKey("dbo.Expedientes", "solcitante_cedula", "dbo.Solicitantes");
            DropIndex("dbo.Expedientes", new[] { "solcitante_cedula" });
            DropIndex("dbo.UsuarioGrupoes", new[] { "Usuario_email" });
            RenameColumn(table: "dbo.UsuarioGrupoes", name: "Usuario_email", newName: "Usuario_Id");
            DropPrimaryKey("dbo.Usuarios");
            DropPrimaryKey("dbo.Solicitantes");
            DropPrimaryKey("dbo.UsuarioGrupoes");
            AddColumn("dbo.Usuarios", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Usuarios", "email", c => c.String(nullable: false));
            AlterColumn("dbo.Usuarios", "password", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.Expedientes", "solcitante_cedula", c => c.String(maxLength: 10));
            AlterColumn("dbo.Solicitantes", "cedula", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.Solicitantes", "nombre", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.Solicitantes", "apellido", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.Solicitantes", "telefono", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.UsuarioGrupoes", "Usuario_Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Usuarios", "Id");
            AddPrimaryKey("dbo.Solicitantes", "cedula");
            AddPrimaryKey("dbo.UsuarioGrupoes", new[] { "Usuario_Id", "Grupo_codGrupo" });
            CreateIndex("dbo.Expedientes", "solcitante_cedula");
            CreateIndex("dbo.UsuarioGrupoes", "Usuario_Id");
            AddForeignKey("dbo.UsuarioGrupoes", "Usuario_Id", "dbo.Usuarios", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Expedientes", "solcitante_cedula", "dbo.Solicitantes", "cedula");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Expedientes", "solcitante_cedula", "dbo.Solicitantes");
            DropForeignKey("dbo.UsuarioGrupoes", "Usuario_Id", "dbo.Usuarios");
            DropIndex("dbo.UsuarioGrupoes", new[] { "Usuario_Id" });
            DropIndex("dbo.Expedientes", new[] { "solcitante_cedula" });
            DropPrimaryKey("dbo.UsuarioGrupoes");
            DropPrimaryKey("dbo.Solicitantes");
            DropPrimaryKey("dbo.Usuarios");
            AlterColumn("dbo.UsuarioGrupoes", "Usuario_Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Solicitantes", "telefono", c => c.String(nullable: false));
            AlterColumn("dbo.Solicitantes", "apellido", c => c.String(nullable: false));
            AlterColumn("dbo.Solicitantes", "nombre", c => c.String(nullable: false));
            AlterColumn("dbo.Solicitantes", "cedula", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Expedientes", "solcitante_cedula", c => c.String(maxLength: 128));
            AlterColumn("dbo.Usuarios", "password", c => c.String());
            AlterColumn("dbo.Usuarios", "email", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.Usuarios", "Id");
            AddPrimaryKey("dbo.UsuarioGrupoes", new[] { "Usuario_email", "Grupo_codGrupo" });
            AddPrimaryKey("dbo.Solicitantes", "cedula");
            AddPrimaryKey("dbo.Usuarios", "email");
            RenameColumn(table: "dbo.UsuarioGrupoes", name: "Usuario_Id", newName: "Usuario_email");
            CreateIndex("dbo.UsuarioGrupoes", "Usuario_email");
            CreateIndex("dbo.Expedientes", "solcitante_cedula");
            AddForeignKey("dbo.Expedientes", "solcitante_cedula", "dbo.Solicitantes", "cedula");
            AddForeignKey("dbo.UsuarioGrupoes", "Usuario_email", "dbo.Usuarios", "email", cascadeDelete: true);
        }
    }
}
