namespace ObligatorioP3Parte2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Etapas",
                c => new
                    {
                        codigoEtapa = c.String(nullable: false, maxLength: 128),
                        lapso = c.Int(nullable: false),
                        descripcion = c.String(),
                        tramite_idTramite = c.Int(),
                    })
                .PrimaryKey(t => t.codigoEtapa)
                .ForeignKey("dbo.Tramites", t => t.tramite_idTramite)
                .Index(t => t.tramite_idTramite);
            
            CreateTable(
                "dbo.Tramites",
                c => new
                    {
                        idTramite = c.Int(nullable: false, identity: true),
                        titulo = c.String(),
                        descripcion = c.String(),
                        costo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        timeEjec = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idTramite);
            
            CreateTable(
                "dbo.Grupoes",
                c => new
                    {
                        codGrupo = c.Int(nullable: false),
                        nombre = c.String(),
                    })
                .PrimaryKey(t => t.codGrupo);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        email = c.String(nullable: false, maxLength: 128),
                        password = c.String(),
                    })
                .PrimaryKey(t => t.email);
            
            CreateTable(
                "dbo.Expedientes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        emailLogueado = c.String(),
                        fechaInicio = c.DateTime(nullable: false),
                        solcitante_cedula = c.String(maxLength: 128),
                        tramite_idTramite = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Solicitantes", t => t.solcitante_cedula)
                .ForeignKey("dbo.Tramites", t => t.tramite_idTramite)
                .Index(t => t.solcitante_cedula)
                .Index(t => t.tramite_idTramite);
            
            CreateTable(
                "dbo.FinalizacionEtapas",
                c => new
                    {
                        idFinalizacionEtapa = c.Int(nullable: false, identity: true),
                        fechaFinaliza = c.DateTime(nullable: false),
                        imagen = c.String(),
                        Finalizo = c.Boolean(nullable: false),
                        etapa_codigoEtapa = c.String(maxLength: 128),
                        Expediente_id = c.Int(),
                        funcionario_email = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.idFinalizacionEtapa)
                .ForeignKey("dbo.Etapas", t => t.etapa_codigoEtapa)
                .ForeignKey("dbo.Expedientes", t => t.Expediente_id)
                .ForeignKey("dbo.Funcionarios", t => t.funcionario_email)
                .Index(t => t.etapa_codigoEtapa)
                .Index(t => t.Expediente_id)
                .Index(t => t.funcionario_email);
            
            CreateTable(
                "dbo.Funcionarios",
                c => new
                    {
                        email = c.String(nullable: false, maxLength: 128),
                        nombre = c.String(),
                        Grupo_codGrupo = c.Int(),
                    })
                .PrimaryKey(t => t.email)
                .ForeignKey("dbo.Grupoes", t => t.Grupo_codGrupo)
                .Index(t => t.Grupo_codGrupo);
            
            CreateTable(
                "dbo.Solicitantes",
                c => new
                    {
                        cedula = c.String(nullable: false, maxLength: 128),
                        nombre = c.String(),
                        apellido = c.String(),
                        telefono = c.String(),
                        email = c.String(),
                    })
                .PrimaryKey(t => t.cedula);
            
            CreateTable(
                "dbo.GrupoTramites",
                c => new
                    {
                        Grupo_codGrupo = c.Int(nullable: false),
                        Tramite_idTramite = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Grupo_codGrupo, t.Tramite_idTramite })
                .ForeignKey("dbo.Grupoes", t => t.Grupo_codGrupo, cascadeDelete: true)
                .ForeignKey("dbo.Tramites", t => t.Tramite_idTramite, cascadeDelete: true)
                .Index(t => t.Grupo_codGrupo)
                .Index(t => t.Tramite_idTramite);
            
            CreateTable(
                "dbo.UsuarioGrupoes",
                c => new
                    {
                        Usuario_email = c.String(nullable: false, maxLength: 128),
                        Grupo_codGrupo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Usuario_email, t.Grupo_codGrupo })
                .ForeignKey("dbo.Usuarios", t => t.Usuario_email, cascadeDelete: true)
                .ForeignKey("dbo.Grupoes", t => t.Grupo_codGrupo, cascadeDelete: true)
                .Index(t => t.Usuario_email)
                .Index(t => t.Grupo_codGrupo);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Expedientes", "tramite_idTramite", "dbo.Tramites");
            DropForeignKey("dbo.Expedientes", "solcitante_cedula", "dbo.Solicitantes");
            DropForeignKey("dbo.FinalizacionEtapas", "funcionario_email", "dbo.Funcionarios");
            DropForeignKey("dbo.Funcionarios", "Grupo_codGrupo", "dbo.Grupoes");
            DropForeignKey("dbo.FinalizacionEtapas", "Expediente_id", "dbo.Expedientes");
            DropForeignKey("dbo.FinalizacionEtapas", "etapa_codigoEtapa", "dbo.Etapas");
            DropForeignKey("dbo.UsuarioGrupoes", "Grupo_codGrupo", "dbo.Grupoes");
            DropForeignKey("dbo.UsuarioGrupoes", "Usuario_email", "dbo.Usuarios");
            DropForeignKey("dbo.GrupoTramites", "Tramite_idTramite", "dbo.Tramites");
            DropForeignKey("dbo.GrupoTramites", "Grupo_codGrupo", "dbo.Grupoes");
            DropForeignKey("dbo.Etapas", "tramite_idTramite", "dbo.Tramites");
            DropIndex("dbo.UsuarioGrupoes", new[] { "Grupo_codGrupo" });
            DropIndex("dbo.UsuarioGrupoes", new[] { "Usuario_email" });
            DropIndex("dbo.GrupoTramites", new[] { "Tramite_idTramite" });
            DropIndex("dbo.GrupoTramites", new[] { "Grupo_codGrupo" });
            DropIndex("dbo.Funcionarios", new[] { "Grupo_codGrupo" });
            DropIndex("dbo.FinalizacionEtapas", new[] { "funcionario_email" });
            DropIndex("dbo.FinalizacionEtapas", new[] { "Expediente_id" });
            DropIndex("dbo.FinalizacionEtapas", new[] { "etapa_codigoEtapa" });
            DropIndex("dbo.Expedientes", new[] { "tramite_idTramite" });
            DropIndex("dbo.Expedientes", new[] { "solcitante_cedula" });
            DropIndex("dbo.Etapas", new[] { "tramite_idTramite" });
            DropTable("dbo.UsuarioGrupoes");
            DropTable("dbo.GrupoTramites");
            DropTable("dbo.Solicitantes");
            DropTable("dbo.Funcionarios");
            DropTable("dbo.FinalizacionEtapas");
            DropTable("dbo.Expedientes");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Grupoes");
            DropTable("dbo.Tramites");
            DropTable("dbo.Etapas");
        }
    }
}
