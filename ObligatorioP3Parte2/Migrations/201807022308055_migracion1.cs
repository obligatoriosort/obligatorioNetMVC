namespace ObligatorioP3Parte2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracion1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Solicitantes", "nombre", c => c.String(nullable: false));
            AlterColumn("dbo.Solicitantes", "apellido", c => c.String(nullable: false));
            AlterColumn("dbo.Solicitantes", "telefono", c => c.String(nullable: false));
            AlterColumn("dbo.Solicitantes", "email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Solicitantes", "email", c => c.String());
            AlterColumn("dbo.Solicitantes", "telefono", c => c.String());
            AlterColumn("dbo.Solicitantes", "apellido", c => c.String());
            AlterColumn("dbo.Solicitantes", "nombre", c => c.String());
        }
    }
}
