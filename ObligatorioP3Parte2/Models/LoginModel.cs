﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObligatorioP3Parte2.Models.Dominio;
using System.ComponentModel.DataAnnotations;

namespace ObligatorioP3Parte2.Models
{
    public class LoginModel
    {
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Correo electrónico")]
        public string EmailLogin { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]

        public string PasswordLogin { get; set; }
    }
}