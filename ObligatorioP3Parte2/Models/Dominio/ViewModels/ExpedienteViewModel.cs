﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObligatorioP3Parte2.Models.Dominio;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ObligatorioP3Parte2.Models.Dominio.ViewModels
{
  public  class ExpedienteViewModel
    {
      
        public Tramite UnTramite { get; set; }
        public SelectList LosTramites { get; set; }
        public int IdTram { get; set; }
        public Solicitante UnSolicitante { get; set; }
        public DateTime fecha { get; set; }

        public ExpedienteViewModel()
        {

            this.UnTramite = new Tramite();
            this.IdTram = UnTramite.idTramite;
            cargarTramites();
            fecha = DateTime.Now;
        }
  

        private void cargarTramites()
        {
            OficinaContext db = new OficinaContext();
            this.LosTramites = new SelectList(db.Tramite.ToList(), "IdTramite", "Titulo");
        }

    }
}
