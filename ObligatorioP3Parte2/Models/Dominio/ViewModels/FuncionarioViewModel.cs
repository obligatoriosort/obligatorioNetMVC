﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObligatorioP3Parte2.Models.Dominio;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ObligatorioP3Parte2.Models.Dominio.ViewModels
{
    public class FuncionarioViewModel
    {
        public Funcionario UnFuncionario { get; set; }
        public FinalizacionEtapa UnaFinalizacion { get; set; }
        public SelectList LosFun{ get; set; }
        public string imagen { get; set; }

        public Tramite tram { get; set; }
        public string email { get; set; }
        


        public FuncionarioViewModel(Tramite tr)
        {
            this.UnFuncionario = new Funcionario();
            this.email = UnFuncionario.email;
            this.tram = tr;
            cargarFuncionarios();
        }
        public FuncionarioViewModel()
        {
            this.UnFuncionario = new Funcionario();
            this.email = UnFuncionario.email;
            this.UnaFinalizacion = new FinalizacionEtapa();
            this.imagen = UnaFinalizacion.imagen;
          

        }

        private void cargarFuncionarios()
        {
            OficinaContext db = new OficinaContext();
            var grupos = from t in db.Tramite
                         from g in t.grupos
                         where t.idTramite == this.tram.idTramite
                         select g;
            grupos = grupos.Distinct();

            List<Grupo> gruposs = grupos.ToList();
            List<Funcionario> funs = new List<Funcionario>();
            foreach (Grupo gp in gruposs)
            {
                var funcionarios = from f in db.Funcionario
                                   where f.Grupo.codGrupo == gp.codGrupo
                                   select f;
                funcionarios = funcionarios.Distinct();
                foreach (Funcionario fu in funcionarios)
                {
                    funs.Add(fu);
                }
            }
           
                               
            this.LosFun= new SelectList(funs, "email", "nombre");
        }

        private bool guardarArchivo(HttpPostedFileBase imagen)
        {
            if (imagen != null)
            {
                string ruta = System.IO.Path.Combine
                (AppDomain.CurrentDomain.BaseDirectory, "Images/fotos");
                if (!System.IO.Directory.Exists(ruta))
                    System.IO.Directory.CreateDirectory(ruta);

                ruta = System.IO.Path.Combine(ruta, imagen.FileName);
                imagen.SaveAs(ruta);
                return true;
            }
            else
                return false;
        }

        //public bool Mapear()
        //{
        //    if (this.imagen != null)
        //    {
        //        if (guardarArchivo(imagen))
        //        {
        //            this.UnaFinalizacion.imagen = imagen.FileName;
        //          //  OficinaContext db = new OficinaContext();
        //          //  this.UnLibro.MiTema = db.Temas.Find(this.IdTemaSeleccionado);
        //            return true;
        //        }
        //    }
        //    return false;
        //}


    }
}