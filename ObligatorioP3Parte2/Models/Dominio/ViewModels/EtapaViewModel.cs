﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObligatorioP3Parte2.Models.Dominio;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ObligatorioP3Parte2.Models.Dominio.ViewModels
{
    public class EtapaViewModel
    {
        public Etapa UnEtapa { get; set; }
        public Tramite UnTramite { get; set; }
        public SelectList LasEtapas { get; set; }
        
        public string IdEtapa { get; set; }
        public int idTram { get; set; }


        public EtapaViewModel(int id)
        {
            this.UnEtapa = new Etapa();
            this.UnTramite = new Tramite();
            this.IdEtapa = UnEtapa.codigoEtapa;
            this.idTram = id;
            cargarEtapas();       
        }
        public EtapaViewModel()
        {
            this.UnEtapa = new Etapa();
            this.UnTramite = new Tramite();
            this.IdEtapa = UnEtapa.codigoEtapa;
            this.idTram = UnTramite.idTramite;
            cargarEtapas();
        }

        private void cargarEtapas()
        {
            using (OficinaContext db = new OficinaContext())
            {
                var etapas = from t in db.Tramite
                                    from e in t.etapas
                                    where t.idTramite==this.idTram
                                    select e;
                etapas = etapas.Distinct();

                this.LasEtapas= new SelectList(etapas.ToList(), "codigoEtapa", "descripcion");
            }
        }
    }
}