﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ObligatorioP3Parte2.Models.Dominio
{
    public class Expediente
    {
        [Key]
        public int id { get; set; }
        public Tramite tramite {get; set;}
        [Display(Name = "Usuario")]
        public string emailLogueado { get; set; }
        public Solicitante solcitante { get; set;}
        public DateTime fechaInicio { get; set; }
        public ICollection<FinalizacionEtapa> etapasFinalizadas { get; set; }
      

    }
}