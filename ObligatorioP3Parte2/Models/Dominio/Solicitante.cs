﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ObligatorioP3Parte2.Models.Dominio
{
    public class Solicitante
    {
        [Key]
        [Required(ErrorMessage = "La cedula es necesaria")]
        [StringLength(10, ErrorMessage = "Supero el limite de caracteres(10)")]
        public string  cedula { get; set; }
        [Required(ErrorMessage = "El nombre es necesaria")]
        [StringLength(15, ErrorMessage = "Supero el limite de caracteres(15)")]
        public string nombre{ get; set; }
        [Required(ErrorMessage = "El apellido es necesaria")]
        [StringLength(15, ErrorMessage = "Supero el limite de caracteres(15)")]
        public string apellido { get; set; }
        [Required(ErrorMessage = "El telefono es necesaria")]
        [StringLength(15, ErrorMessage = "Supero el limite de caracteres(15)")]
        public string  telefono { get; set; }
        [Required(ErrorMessage = "El email es necesaria")]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$",
            ErrorMessage = "Formato correo invalido.")]
        public string  email { get; set; }

    }
}