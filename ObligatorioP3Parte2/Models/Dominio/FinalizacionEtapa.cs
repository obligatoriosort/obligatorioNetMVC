﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ObligatorioP3Parte2.Models.Dominio
{
    public class FinalizacionEtapa
    {
        [Key]
        public int idFinalizacionEtapa { get; set; }
        public Etapa etapa { get; set; }
        public DateTime fechaFinaliza { get; set; }
        public string imagen { get; set; }
        public Funcionario funcionario { get; set; }
        public Expediente Expediente { get; set; }
        [Display(Name = "En Fecha")]
        public bool Finalizo { get; set; }

    }
}