﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ObligatorioP3Parte2.Models.Dominio
{
    
    public class Grupo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codGrupo { get; set; }
        public string nombre { get; set; }
        public virtual ICollection<Usuario> usuarios { get; set; }
        public virtual ICollection<Tramite> tramites { get; set; }
    }
}