﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ObligatorioP3Parte2.Models.Dominio
{
    public class Tramite
    {
        [Key]
        public int idTramite{ get; set; }
        public string  titulo { get; set; }
        public string descripcion{ get; set; }
        public decimal  costo { get; set; }
        public int timeEjec { get; set; }
        public virtual ICollection<Grupo> grupos { get; set; }
        public virtual ICollection<Etapa> etapas { get; set; }
        



    }
}