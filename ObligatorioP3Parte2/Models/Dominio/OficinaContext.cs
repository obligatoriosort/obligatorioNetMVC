﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
namespace ObligatorioP3Parte2.Models.Dominio
{
    public class OficinaContext : DbContext
    {
        public DbSet<Grupo> Grupos { get; set; }
        public DbSet<Funcionario> Funcionario { get; set; }
        public DbSet<Tramite> Tramite { get; set; }
        public DbSet<Etapa> Etapa { get; set; }
        public DbSet<Expediente> Expediente { get; set; }
        public DbSet<FinalizacionEtapa> FinalizacionEtapa { get; set; }
        public DbSet<Solicitante> Solicitante { get; set; }
        public DbSet<Usuario> Usuario { get; set; }


        public OficinaContext() : base("con")
        {

        }

       
       protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
       


    }
}
