﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ObligatorioP3Parte2.Models.Dominio
{
    public class Usuario
    {
        [Key]
       
        public int Id { get; set; }
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$",
           ErrorMessage = "Formato Invalido")]
        [Required(ErrorMessage = "Campo requerido.")]
        public string  email { get; set; }
        [Required(ErrorMessage = "Campo requerido.")]
        [StringLength(10,ErrorMessage ="Supero el limite de caracteres(10)")]
        [DataType(DataType.Password)]

        public string  password { get; set; }
        public virtual ICollection<Grupo> grupos { get; set; }
       
    }
}