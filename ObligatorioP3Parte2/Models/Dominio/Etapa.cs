﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ObligatorioP3Parte2.Models.Dominio
{
    public class Etapa
    {
        [Key]
        public string codigoEtapa { get; set; }
        public int lapso { get; set; }
        public string descripcion { get; set; }

        public Tramite tramite { get; set; }

    }
}