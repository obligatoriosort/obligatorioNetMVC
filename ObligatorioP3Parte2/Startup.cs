﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ObligatorioP3Parte2.Startup))]
namespace ObligatorioP3Parte2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
